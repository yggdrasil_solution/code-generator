package model.bo.cg;

import static cococare.common.CCClass.getCCField;
import static cococare.common.CCFormat.countOccurrencesOf;
import static cococare.common.CCFormat.replaceFirst;
import cococare.database.CCHibernateBo;
import java.util.Arrays;
import java.util.List;
import model.dao.cg.CgEntityRuleDao;
import model.dao.cg.CgEntityRuleItemDao;
import model.dao.cg.CgGeneratorRuleDao;
import model.obj.cg.CgCodeGenerator;
import model.obj.cg.CgEntityRule;
import model.obj.cg.CgEntityRuleItem;
import model.obj.cg.CgFinal.ComponentType;
import model.obj.cg.CgFinal.RepeatType;
import model.obj.cg.CgFinal.ReplaceBy;
import model.obj.cg.CgGeneratorRule;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class CgCodeGeneratorBo extends CCHibernateBo {

    private CgEntityRuleItemDao entityRuleItemDao;
    //
    private CgGeneratorRuleDao generatorRuleDao;
    private CgEntityRuleDao entityRuleDao;

    public synchronized List<CgEntityRuleItem> getEntityRuleItems(CgEntityRule entityRule) {
        return entityRuleItemDao.getEntityRuleItems(entityRule);
    }

    protected class CgParam {

        protected CgGeneratorRule generatorRule;
        protected RepeatType repeatType;
        protected int totalPattern = 1;
        protected String[] fieldPatterns = null;
        protected String bulkCode = "";
        protected String[] fieldCode = null;

        protected CgParam(CgGeneratorRule generatorRule) {
            this.generatorRule = generatorRule;
            this.repeatType = RepeatType.values()[generatorRule.getRepeatTypeIndex()];
            if (RepeatType.FIELDS_TO_PATTERN.equals(this.repeatType)) {
                this.totalPattern = countOccurrencesOf(generatorRule.getPattern(), "[]");
                this.fieldPatterns = generatorRule.getFieldPattern().split("\\|");
                this.fieldCode = new String[this.totalPattern];
                Arrays.fill(this.fieldCode, "");
            }
        }
    }

    private synchronized String[] _getReplacementValues(CgGeneratorRule generatorRule, CgEntityRuleItem entityRuleItem, int segment, boolean dscp) {
        String replacements = generatorRule.getReplacements();
        if (dscp) {
            replacements = replacements.
                    replaceAll("fieldNameCd", "fieldNameDscp").
                    replaceAll("columnNameCd", "columnNameDscp");
        }
        String[] replacementSegments = replacements.split("\\|");
        String[] replacementFields = replacementSegments[segment].split(",");
        String[] replacementValues = new String[replacementFields.length];
        for (int i = 0; i < replacementFields.length; i++) {
            replacementValues[i] = getCCField(entityRuleItem, replacementFields[i]).getValue4Data();
        }
        return replacementValues;
    }

    private synchronized void _getGeneratedCode(CgParam param, CgEntityRuleItem entityRuleItem, boolean dscp) {
        if (RepeatType.ONE_FIELD_ONE_PATTERN.equals(param.repeatType)) {
            param.bulkCode += replaceFirst(param.generatorRule.getPattern(), _getReplacementValues(param.generatorRule, entityRuleItem, 0, dscp));
        } else if (RepeatType.FIELDS_TO_PATTERN.equals(param.repeatType)) {
            for (int i = 0; i < param.totalPattern; i++) {
                param.fieldCode[i] += replaceFirst(param.fieldPatterns[i], _getReplacementValues(param.generatorRule, entityRuleItem, i, dscp));
            }
        }
    }

    private synchronized String _getGeneratedCode(CgGeneratorRule generatorRule, CgEntityRule entityRule, String fieldName) {
        String result = "";
        ReplaceBy replaceBy = ReplaceBy.values()[generatorRule.getReplaceByIndex()];
        if (ReplaceBy.RULE.equals(replaceBy)) {
            CgParam param = new CgParam(generatorRule);
            List<CgEntityRuleItem> entityRuleItems = getEntityRuleItems(entityRule);
            for (CgEntityRuleItem entityRuleItem : entityRuleItems) {
                ComponentType componentType = ComponentType.values()[entityRuleItem.getComponentTypeIndex()];
                if (!generatorRule.getInputOnly() || componentType.isInput()) {
                    _getGeneratedCode(param, entityRuleItem, false);
                    if (generatorRule.getGenerateDscp() && componentType.isFk()
                            && (generatorRule.getReplacements().contains("fieldNameCd")
                            || generatorRule.getReplacements().contains("columnNameCd"))) {
                        _getGeneratedCode(param, entityRuleItem, true);
                    }
                }
            }
            if (RepeatType.FIELDS_TO_PATTERN.equals(param.repeatType)) {
                param.bulkCode += replaceFirst(generatorRule.getPattern(), param.fieldCode);
            }
            result = param.bulkCode;
        } else if (ReplaceBy.FIELD.equals(replaceBy)) {
            String bulkCode = generatorRule.getPattern().replaceAll("\\[\\]", fieldName);
            result = bulkCode;
        }
        return result;
    }

    public synchronized void doGenerateCode(CgCodeGenerator codeGenerator) {
        if (!codeGenerator.getAllGeneratorRule() && !codeGenerator.getAllEntityRule()) {
            codeGenerator.setResult(_getGeneratedCode(codeGenerator.getGeneratorRule(), codeGenerator.getEntityRule(), codeGenerator.getFieldName()));
        } else {
            String result = "";
            List<CgGeneratorRule> generatorRules =
                    codeGenerator.getAllGeneratorRule()
                    ? generatorRuleDao.getGeneratorRules(codeGenerator)
                    : Arrays.asList(codeGenerator.getGeneratorRule());
            List<CgEntityRule> entityRules =
                    codeGenerator.getAllEntityRule()
                    ? entityRuleDao.getEntityRules(codeGenerator)
                    : Arrays.asList(codeGenerator.getEntityRule());
            for (CgGeneratorRule generatorRule : generatorRules) {
                for (CgEntityRule entityRule : entityRules) {
                    result += _getGeneratedCode(generatorRule, entityRule, codeGenerator.getFieldName());
                    result += "\n\n";
                }
            }
            codeGenerator.setResult(result);
        }
    }
}