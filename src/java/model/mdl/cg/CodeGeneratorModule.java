package model.mdl.cg;

import cococare.database.CCHibernateModule;
import java.util.Arrays;
import java.util.List;
import model.obj.cg.CgEntityRule;
import model.obj.cg.CgEntityRuleItem;
import model.obj.cg.CgGeneratorRule;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class CodeGeneratorModule extends CCHibernateModule {

    public static CodeGeneratorModule INSTANCE = new CodeGeneratorModule();

    @Override
    protected List<Class> _getAnnotatedClasses() {
        return (List) Arrays.asList(
                //parameter
                CgGeneratorRule.class,
                CgEntityRule.class,
                CgEntityRuleItem.class);
    }
}