package model.mdl.cg;

import static cococare.common.CCConfig.*;
import static cococare.common.CCLanguage.*;
import cococare.framework.common.CFApplUae;
import cococare.framework.zk.CFZkMain;
import cococare.framework.zk.controller.zul.util.ZulDatabaseSettingCtrl;
import cococare.framework.zk.controller.zul.util.ZulExportImportCtrl;
import cococare.framework.zk.controller.zul.util.ZulParameterListCtrl;
import controller.zul.cg.ZulCodeGeneratorCtrl;
import controller.zul.cg.ZulExportRulesToXlsCtrl;
import controller.zul.cg.ZulImportRulesFromXlsCtrl;
import static model.obj.cg.CgFinal.*;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class CodeGeneratorMain extends CFZkMain {

    @Override
    protected void _loadInternalSetting() {
        MSG_SHOW_PRINTLN = true;
        MSG_SHOW_LOG_INFO = true;
        MSG_SHOW_LOG_DEBUG = true;
        MSG_SHOW_LOG_ERROR = true;
        EDT_COMP_LABEL_WIDTH = 150;
        HBN_SHOW_HQL = "true";
        TBL_COLUMN_MAX_WIDTH = 150;
        //
        LICENSE_ACTIVE = false;
        APPL_ID = "code-generator";
        APPL_CODE = "code-generator";
        APPL_LOGO = "/cococare/resource/icon-cococare.jpg";
        APPL_NAME = "Code-Generator";
        APPL_VER = "2.0.190317";
        super._loadInternalSetting();
    }

    @Override
    protected void _loadExternalSetting() {
        super._loadExternalSetting();
    }

    @Override
    protected void _initDatabaseEntity() {
        super._initDatabaseEntity();
        CodeGeneratorModule.INSTANCE.init(HIBERNATE);
    }

    @Override
    protected boolean _initInitialData() {
        return super._initInitialData();
    }

    @Override
    protected void _initInitialUaeBody(CFApplUae uae) {
    }

    @Override
    protected boolean _initInitialUaeEnd(CFApplUae uae) {
        if (!HIBERNATE.getParameterClasses().isEmpty()) {
            uae.reg(Utility, Parameter, ZulParameterListCtrl.class);
            uae.reg(Utility, Export_Import, ZulExportImportCtrl.class);
        }
        uae.reg(Utility, Database_Setting, ZulDatabaseSettingCtrl.class);
        return uae.compile();
    }

    @Override
    protected void _applyUserConfigUaeBody(CFApplUae uae) {
        uae.addMenuRoot(
                ZulCodeGeneratorCtrl.class,
                ZulExportRulesToXlsCtrl.class,
                ZulImportRulesFromXlsCtrl.class);
        uae.addMenuParent(CODE_GENERATOR, null, ZulCodeGeneratorCtrl.class);
        uae.addMenuParent(EXPORT_RULES_TO_XLS, null, ZulExportRulesToXlsCtrl.class);
        uae.addMenuParent(IMPORT_RULES_FROM_XLS, null, ZulImportRulesFromXlsCtrl.class);
    }
}