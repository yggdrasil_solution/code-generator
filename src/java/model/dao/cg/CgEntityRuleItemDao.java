package model.dao.cg;

import java.util.List;
import model.mdl.cg.CodeGeneratorDao;
import model.obj.cg.CgEntityRule;
import model.obj.cg.CgEntityRuleItem;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class CgEntityRuleItemDao extends CodeGeneratorDao {

    @Override
    protected Class getEntity() {
        return CgEntityRuleItem.class;
    }

    public List<CgEntityRuleItem> getEntityRuleItems(CgEntityRule entityRule) {
        hql.start().
                where("entityRule = :entityRule").
                orderBy("id ASC");
        parameters.start().
                set("entityRule", entityRule);
        return getListBy(hql.value(), parameters.value());
    }
}