package model.dao.cg;

import java.util.ArrayList;
import java.util.List;
import model.mdl.cg.CodeGeneratorDao;
import model.obj.cg.CgCodeGenerator;
import model.obj.cg.CgFinal.ReplaceBy;
import model.obj.cg.CgGeneratorRule;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class CgGeneratorRuleDao extends CodeGeneratorDao {

    @Override
    protected Class getEntity() {
        return CgGeneratorRule.class;
    }

    public List<CgGeneratorRule> getGeneratorRules(CgCodeGenerator codeGenerator) {
        List<Integer> replaceByIndex = new ArrayList();
        if (!codeGenerator.getGeneratorRuleByRule() && !codeGenerator.getGeneratorRuleByField()) {
            replaceByIndex.add(ReplaceBy.RULE.ordinal());
            replaceByIndex.add(ReplaceBy.FIELD.ordinal());
        } else if (codeGenerator.getGeneratorRuleByRule()) {
            replaceByIndex.add(ReplaceBy.RULE.ordinal());
        } else if (codeGenerator.getGeneratorRuleByField()) {
            replaceByIndex.add(ReplaceBy.FIELD.ordinal());
        }
        hql.start().
                where("replaceByIndex IN (:replaceByIndex)").
                orderBy("name ASC");
        parameters.start().
                set("replaceByIndex", replaceByIndex);
        return getListBy(hql.value(), parameters.value());
    }
}