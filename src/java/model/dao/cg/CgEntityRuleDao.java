package model.dao.cg;

import java.util.List;
import model.mdl.cg.CodeGeneratorDao;
import model.obj.cg.CgCodeGenerator;
import model.obj.cg.CgEntityRule;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class CgEntityRuleDao extends CodeGeneratorDao {

    @Override
    protected Class getEntity() {
        return CgEntityRule.class;
    }

    public List<CgEntityRule> getEntityRules(CgCodeGenerator codeGenerator) {
        hql.start().
                orderBy("name ASC");
        parameters.start();
        return getListBy(hql.value(), parameters.value());
    }
}