package model.obj.cg;

import cococare.common.CCFieldConfig;
import cococare.common.CCFieldConfig.AccessValue;
import cococare.common.CCFieldConfig.Accessible;
import static cococare.common.CCFormat.*;
import static cococare.common.CCLogic.isNotNullAndNotEmpty;
import static cococare.common.CCLogic.isNullOrEmpty;
import cococare.common.CCTypeConfig;
import cococare.database.CCEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import static model.obj.cg.CgFinal.*;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
@Entity
@Table(name = "cg_entity_rule_items")
@CCTypeConfig(label = "Entity Rule Item", uniqueKey = "label")
public class CgEntityRuleItem extends CCEntity {

    @ManyToOne
    @CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
    private CgEntityRule entityRule;
    @Column(length = 50)
    @CCFieldConfig(accessible = Accessible.MANDATORY, unique = true, requestFocus = true)
    private String label;
    @CCFieldConfig(label = "Component Type", accessible = Accessible.MANDATORY, optionSource = "model.obj.cg.CgFinal$ComponentType", optionReflectKey = "componentType", visible = false)
    private Integer componentTypeIndex = 2;
    @Column(length = 16)
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false, visible2 = false)
    private String componentType;
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false, visible2 = false)
    transient private String componentLength;
    @Column(name = "length_", length = 8)
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false)
    private String length;
    @CCFieldConfig(label = "Accessible", accessible = Accessible.MANDATORY, optionSource = "model.obj.cg.CgFinal$Accessible", optionReflectKey = "accessible", visible = false)
    private Integer accessibleIndex = 0;
    @Column(name = "accessible_", length = 16)
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false, visible2 = false)
    private String accessible;
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible2 = false)
    transient private String screenRule;
    @CCFieldConfig(label = "Field Type", accessible = Accessible.MANDATORY, optionSource = "model.obj.cg.CgFinal$FieldType", optionReflectKey = "fieldType", visible = false)
    private Integer fieldTypeIndex = 10;
    @Column(length = 16)
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible2 = false)
    private String fieldType;
    @Column(length = 50)
    @CCFieldConfig(accessValue = AccessValue.METHOD, accessible = Accessible.NORMAL)
    private String fieldName;
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false, visible2 = false)
    transient private String fieldNameS;
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false, visible2 = false)
    transient private String fieldNameCd;
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false, visible2 = false)
    transient private String fieldNameDscp;
    @Column(length = 50)
    @CCFieldConfig(accessValue = AccessValue.METHOD, accessible = Accessible.NORMAL)
    private String columnName;
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false, visible2 = false)
    transient private String columnNameCd;
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false, visible2 = false)
    transient private String columnNameDscp;
    @CCFieldConfig(label = "Column Type", accessible = Accessible.MANDATORY, optionSource = "model.obj.cg.CgFinal$ColumnType", optionReflectKey = "columnType", visible = false)
    private Integer columnTypeIndex = 3;
    @Column(length = 16)
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible2 = false)
    private String columnType;
    @CCFieldConfig(accessValue = AccessValue.METHOD, visible = false, visible2 = false)
    transient private String columnLength;

    public CgEntityRule getEntityRule() {
        return entityRule;
    }

    public void setEntityRule(CgEntityRule entityRule) {
        this.entityRule = entityRule;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getComponentTypeIndex() {
        return componentTypeIndex;
    }

    public void setComponentTypeIndex(Integer componentTypeIndex) {
        this.componentTypeIndex = componentTypeIndex;
    }

    public String getComponentType() {
        if (isNullOrEmpty(componentType)) {
            componentType = DEFAULT_COMPONENT_TYPE;
        }
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public String getComponentLength() {
        if (ComponentType.TEXT_BOX.equals(ComponentType.values()[componentTypeIndex])) {
            if (isFieldTypeIsNumeric()) {
                String[] lengths = getLength().split(",");
                int length0 = getInteger(lengths[0]);
                int length1 = lengths.length > 1 ? getInteger(lengths[1]) : 0;
                length0 = length0 - length1;
                length0 = length0 + (length0 / 3);
                length0 = length0 + 1 + length1;
                componentLength = getString(length0);
            } else {
                componentLength = getLength();
            }
        } else {
            componentLength = "";
        }
        return componentLength;
    }

    public void setComponentLength(String componentLength) {
        this.componentLength = componentLength;
    }

    public String getLength() {
        if (isNullOrEmpty(length)) {
            length = DEFAULT_LENGTH;
        }
        if (length.contains(".")) {
            length = getString(parseInt(length));
        }
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public Integer getAccessibleIndex() {
        return accessibleIndex;
    }

    public void setAccessibleIndex(Integer accessibleIndex) {
        this.accessibleIndex = accessibleIndex;
    }

    public String getAccessible() {
        if (isNullOrEmpty(accessible)) {
            accessible = DEFAULT_ACCESSIBLE;
        }
        return accessible;
    }

    public void setAccessible(String accessible) {
        this.accessible = accessible;
    }

    public String getScreenRule() {
        if (isNullOrEmpty(screenRule)) {
            screenRule = replaceFirst("[]([])[]", getComponentType(), getLength(), getAccessible());
        }
        return screenRule;
    }

    public void setScreenRule(String screenRule) {
        this.screenRule = screenRule;
    }

    public Integer getFieldTypeIndex() {
        return fieldTypeIndex;
    }

    public void setFieldTypeIndex(Integer fieldTypeIndex) {
        this.fieldTypeIndex = fieldTypeIndex;
    }

    public String getFieldType() {
        if (isNullOrEmpty(fieldType)) {
            fieldType = DEFAULT_FIELD_TYPE;
        }
        return fieldType;
    }

    public boolean isFieldTypeIsString() {
        return "String".equalsIgnoreCase(getFieldType());
    }

    public boolean isFieldTypeIsNumeric() {
        return "BigDecimal".equalsIgnoreCase(getFieldType());
    }

    public boolean isFieldTypeIsDate() {
        return "Date".equalsIgnoreCase(getFieldType());
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldName() {
        if (isNullOrEmpty(fieldName) && isNotNullAndNotEmpty(label)) {
            fieldName = label.replaceAll("\\W", "");
            fieldName = fieldName.substring(0, 1).toLowerCase() + fieldName.substring(1, fieldName.length());
        }
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldNameS() {
        fieldNameS = toSentenceCase(getFieldName(), false, false);
        return fieldNameS;
    }

    public void setFieldNameS(String fieldNameS) {
        this.fieldNameS = fieldNameS;
    }

    public String getFieldNameCd() {
        fieldNameCd = getFieldName();
        if (ComponentType.values()[componentTypeIndex].isFk()) {
            fieldNameCd += FIELD_NAME_CD;
        }
        return fieldNameCd;
    }

    public void setFieldNameCd(String fieldNameCd) {
        this.fieldNameCd = fieldNameCd;
    }

    public String getFieldNameDscp() {
        fieldNameDscp = getFieldName();
        if (ComponentType.values()[componentTypeIndex].isFk()) {
            fieldNameDscp += FIELD_NAME_DSCP;
        }
        return fieldNameDscp;
    }

    public void setFieldNameDscp(String fieldNameDscp) {
        this.fieldNameDscp = fieldNameDscp;
    }

    public String getColumnName() {
        if (isNullOrEmpty(columnName)) {
            columnName = label.replaceAll("\\W+", " ").trim().replaceAll(" ", "_");
            columnName = columnName.toUpperCase();
        }
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnNameCd() {
        columnNameCd = getColumnName();
        if (ComponentType.values()[componentTypeIndex].isFk()) {
            columnNameCd += COLUMN_NAME_CD;
        }
        return columnNameCd;
    }

    public void setColumnNameCd(String columnNameCd) {
        this.columnNameCd = columnNameCd;
    }

    public String getColumnNameDscp() {
        columnNameDscp = getColumnName();
        if (ComponentType.values()[componentTypeIndex].isFk()) {
            columnNameDscp += COLUMN_NAME_DSCP;
        }
        return columnNameDscp;
    }

    public void setColumnNameDscp(String columnNameDscp) {
        this.columnNameDscp = columnNameDscp;
    }

    public Integer getColumnTypeIndex() {
        return columnTypeIndex;
    }

    public void setColumnTypeIndex(Integer columnTypeIndex) {
        this.columnTypeIndex = columnTypeIndex;
    }

    public String getColumnType() {
        if (isNullOrEmpty(columnType)) {
            columnType = DEFAULT_COLUMNT_TYPE;
        }
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnLength() {
        if (ColumnType.NUMBER.equals(ColumnType.values()[columnTypeIndex])
                || ColumnType.NVARCHAR2.equals(ColumnType.values()[columnTypeIndex])) {
            columnLength = "(" + getLength() + ")";
        } else {
            columnLength = "";
        }
        return columnLength;
    }

    public void setColumnLength(String columnLength) {
        this.columnLength = columnLength;
    }
}