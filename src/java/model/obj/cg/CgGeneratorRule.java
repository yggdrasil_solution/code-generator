package model.obj.cg;

import cococare.common.CCFieldConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCTypeConfig;
import cococare.database.CCEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
@Entity
@Table(name = "cg_generator_rules")
@CCTypeConfig(label = "Generator Rule", uniqueKey = "name", parameter = true, controllerClass = "controller.pseudo.cg.CgGeneratorRuleCtrl")
public class CgGeneratorRule extends CCEntity {

    @Column(length = 32)
    @CCFieldConfig(accessible = Accessible.MANDATORY, unique = true, requestFocus = true)
    private String name;
    @Column(length = Short.MAX_VALUE)
    @CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = Short.MAX_VALUE, visible = false)
    private String pattern;
    @CCFieldConfig(label = "Replace By", accessible = Accessible.MANDATORY, optionSource = "model.obj.cg.CgFinal$ReplaceBy", optionReflectKey = "replaceBy", visible = false)
    private Integer replaceByIndex = 0;
    @Column(length = 16)
    @CCFieldConfig(visible2 = false)
    private String replaceBy;
    @CCFieldConfig(accessible = Accessible.NORMAL, maxLength = 4)
    private Boolean inputOnly;
    @CCFieldConfig(accessible = Accessible.NORMAL, maxLength = 4)
    private Boolean generateDscp;
    @CCFieldConfig(label = "Repeat Type", accessible = Accessible.MANDATORY, optionSource = "model.obj.cg.CgFinal$RepeatType", optionReflectKey = "repeatType", visible = false)
    private Integer repeatTypeIndex = 0;
    @Column(length = 24)
    @CCFieldConfig(visible2 = false)
    private String repeatType;
    @Column(length = Short.MAX_VALUE)
    @CCFieldConfig(tooltiptext = "Entity Rule Item Fields (Comma Separator)", accessible = Accessible.MANDATORY, maxLength = Short.MAX_VALUE, visible = false)
    private String replacements;
    @Column(length = Short.MAX_VALUE)
    @CCFieldConfig(tooltiptext = "Used by Each Entity Rule Item (Pipe Separator)", accessible = Accessible.MANDATORY, maxLength = Short.MAX_VALUE, visible = false)
    private String fieldPattern;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Integer getReplaceByIndex() {
        return replaceByIndex;
    }

    public void setReplaceByIndex(Integer replaceByIndex) {
        this.replaceByIndex = replaceByIndex;
    }

    public String getReplaceBy() {
        return replaceBy;
    }

    public void setReplaceBy(String replaceBy) {
        this.replaceBy = replaceBy;
    }

    public Boolean getInputOnly() {
        return inputOnly;
    }

    public void setInputOnly(Boolean inputOnly) {
        this.inputOnly = inputOnly;
    }

    public Boolean getGenerateDscp() {
        return generateDscp;
    }

    public void setGenerateDscp(Boolean generateDscp) {
        this.generateDscp = generateDscp;
    }

    public Integer getRepeatTypeIndex() {
        return repeatTypeIndex;
    }

    public void setRepeatTypeIndex(Integer repeatTypeIndex) {
        this.repeatTypeIndex = repeatTypeIndex;
    }

    public String getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(String repeatType) {
        this.repeatType = repeatType;
    }

    public String getReplacements() {
        return replacements;
    }

    public void setReplacements(String replacements) {
        this.replacements = replacements;
    }

    public String getFieldPattern() {
        return fieldPattern;
    }

    public void setFieldPattern(String fieldPattern) {
        this.fieldPattern = fieldPattern;
    }
}