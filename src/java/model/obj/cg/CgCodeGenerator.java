package model.obj.cg;

import cococare.common.CCFieldConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.ComponentType;
import cococare.database.CCEntity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class CgCodeGenerator extends CCEntity {

    @ManyToOne
    @CCFieldConfig(accessible = Accessible.MANDATORY, componentType = ComponentType.COMBOBOX, maxLength = 32, uniqueKey = "name")
    private CgGeneratorRule generatorRule;
    @CCFieldConfig(visible = false, visible2 = false)
    private Boolean allGeneratorRule;
    @CCFieldConfig(visible = false, visible2 = false)
    private Boolean generatorRuleByRule;
    @CCFieldConfig(visible = false, visible2 = false)
    private Boolean generatorRuleByField;
    @ManyToOne
    @CCFieldConfig(accessible = Accessible.MANDATORY, componentType = ComponentType.COMBOBOX, maxLength = 32, uniqueKey = "name")
    private CgEntityRule entityRule;
    @CCFieldConfig(visible = false, visible2 = false)
    private Boolean allEntityRule;
    @Column(length = 32)
    @CCFieldConfig(accessible = Accessible.MANDATORY)
    private String fieldName;
    @Column(length = Short.MAX_VALUE)
    @CCFieldConfig(accessible = Accessible.NORMAL, maxLength = Short.MAX_VALUE)
    private String result;

    public CgGeneratorRule getGeneratorRule() {
        return generatorRule;
    }

    public void setGeneratorRule(CgGeneratorRule generatorRule) {
        this.generatorRule = generatorRule;
    }

    public Boolean getAllGeneratorRule() {
        return allGeneratorRule;
    }

    public void setAllGeneratorRule(Boolean allGeneratorRule) {
        this.allGeneratorRule = allGeneratorRule;
    }

    public Boolean getGeneratorRuleByRule() {
        return generatorRuleByRule;
    }

    public void setGeneratorRuleByRule(Boolean generatorRuleByRule) {
        this.generatorRuleByRule = generatorRuleByRule;
    }

    public Boolean getGeneratorRuleByField() {
        return generatorRuleByField;
    }

    public void setGeneratorRuleByField(Boolean generatorRuleByField) {
        this.generatorRuleByField = generatorRuleByField;
    }

    public CgEntityRule getEntityRule() {
        return entityRule;
    }

    public void setEntityRule(CgEntityRule entityRule) {
        this.entityRule = entityRule;
    }

    public Boolean getAllEntityRule() {
        return allEntityRule;
    }

    public void setAllEntityRule(Boolean allEntityRule) {
        this.allEntityRule = allEntityRule;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}