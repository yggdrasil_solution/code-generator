package model.obj.cg.xls;

import cococare.common.CCFieldConfig;
import cococare.common.CCFieldConfig.AccessValue;
import static cococare.common.CCFormat.getString;
import static cococare.common.CCFormat.parseInt;
import static cococare.common.CCLogic.isNotNullAndNotEmpty;
import static cococare.common.CCLogic.isNullOrEmpty;
import cococare.database.CCEntity;
import static model.obj.cg.CgFinal.*;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class CgEntityRuleData extends CCEntity {

    @CCFieldConfig()
    private String label;
    @CCFieldConfig(accessValue = AccessValue.METHOD)
    private String componentType;
    @CCFieldConfig(accessValue = AccessValue.METHOD)
    private String length;
    @CCFieldConfig(accessValue = AccessValue.METHOD)
    private String accessible;
    @CCFieldConfig(accessValue = AccessValue.METHOD)
    private String fieldType;
    @CCFieldConfig(accessValue = AccessValue.METHOD)
    private String fieldName;
    @CCFieldConfig(accessValue = AccessValue.METHOD)
    private String columnName;
    @CCFieldConfig(accessValue = AccessValue.METHOD)
    private String columnType;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getComponentType() {
        if (isNullOrEmpty(componentType)) {
            componentType = DEFAULT_COMPONENT_TYPE;
        }
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public String getLength() {
        if (isNullOrEmpty(length)) {
            length = DEFAULT_LENGTH;
        }
        if (length.contains(".")) {
            length = getString(parseInt(length));
        }
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getAccessible() {
        if (isNullOrEmpty(accessible)) {
            accessible = DEFAULT_ACCESSIBLE;
        }
        return accessible;
    }

    public void setAccessible(String accessible) {
        this.accessible = accessible;
    }

    public String getFieldType() {
        if (isNullOrEmpty(fieldType)) {
            fieldType = DEFAULT_FIELD_TYPE;
        }
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldName() {
        if (isNullOrEmpty(fieldName) && isNotNullAndNotEmpty(label)) {
            fieldName = label.replaceAll("\\W", "");
            fieldName = fieldName.substring(0, 1).toLowerCase() + fieldName.substring(1, fieldName.length());
        }
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getColumnName() {
        if (isNullOrEmpty(columnName)) {
            columnName = label.replaceAll("\\W+", " ").trim().replaceAll(" ", "_");
            columnName = columnName.toUpperCase();
        }
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnType() {
        if (isNullOrEmpty(columnType)) {
            columnType = DEFAULT_COLUMNT_TYPE;
        }
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }
}