package model.obj.cg;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class CgFinal {

    public static final String DEFAULT_COMPONENT_TYPE = "Text Box";
    public static final String DEFAULT_LENGTH = "2";
    public static final String DEFAULT_ACCESSIBLE = "Normal";
    public static final String DEFAULT_FIELD_TYPE = "String";
    public static final String DEFAULT_COLUMNT_TYPE = "NVARCHAR2";
    public static final String COLUMN_NAME_CD = "_CD";
    public static final String COLUMN_NAME_DSCP = "_DSCP";
    public static final String FIELD_NAME_CD = "Cd";
    public static final String FIELD_NAME_DSCP = "Dscp";
    //
    public static final String CODE_GENERATOR = "Code Generator";
    public static final String GENERATE_CODE = "Generate Code";
    public static final String EXPORT_RULES_TO_XLS = "Export Rules To Xls";
    public static final String EXPORT_GEN_RULES_TO_XLS = "Export Generator Rules To Xls";
    public static final String EXPORT_ENT_RULES_TO_XLS = "Export Entity Rules To Xls";
    public static final String IMPORT_RULES_FROM_XLS = "Import Rules From Xls";
    public static final String IMPORT_GEN_RULES_FROM_XLS = "Import Generator Rules From Xls";
    public static final String IMPORT_ENT_RULES_FROM_XLS = "Import Entity Rules From Xls";
    //
    public static final String GENERATOR_RULES = "GeneratorRules";
    public static final String ENTITY_RULES = "EntityRules";

    public enum ComponentType {

        HEADER("Header", false, false),
        LABEL("Label", true, false),
        TEXT_BOX("Text Box", true, false),
        DATE_PICKER("Date picker", true, false),
        RADIO_BUTTON("Radio Button", true, false),
        DROP_LIST("Drop List", true, true),
        PICK_LIST("Pick List", true, true),
        BUTTON("Button", false, false);
        private String value;
        private boolean input;
        private boolean fk;

        private ComponentType(String value, boolean input, boolean fk) {
            this.value = value;
            this.input = input;
            this.fk = fk;
        }

        @Override
        public String toString() {
            return value;
        }

        public boolean isInput() {
            return input;
        }

        public boolean isFk() {
            return fk;
        }
    }

    public enum Accessible {

        NORMAL, MANDATORY, READONLY;
    }

    public enum FieldType {

        BigDecimal, BigInteger, Boolean, Byte, Date, Double, Float, Integer, Long, Short, String, Time;
    }

    public enum ColumnType {

        CLOB, DATE, NUMBER, NVARCHAR2, TIMESTAMP;
    }

    public enum ReplaceBy {

        RULE, FIELD;
    }

    public enum RepeatType {

        ONE_FIELD_ONE_PATTERN, FIELDS_TO_PATTERN;
    }
}