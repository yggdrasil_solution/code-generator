package model.obj.cg;

import cococare.common.CCFieldConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCTypeConfig;
import cococare.database.CCEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
@Entity
@Table(name = "cg_entity_rules")
@CCTypeConfig(label = "Entity Rule", uniqueKey = "name", parameter = true, controllerClass = "controller.pseudo.cg.CgEntityRuleCtrl")
public class CgEntityRule extends CCEntity {

    @Column(length = 32)
    @CCFieldConfig(accessible = Accessible.MANDATORY)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}