package controller.zul.cg;

import cococare.common.CCFieldConfig.Accessible;
import static cococare.common.CCLogic.isNotNull;
import cococare.framework.zk.controller.zul.ZulDefaultCtrl;
import cococare.zk.CCCombobox;
import static cococare.zk.CCZk.*;
import model.bo.cg.CgCodeGeneratorBo;
import model.obj.cg.CgCodeGenerator;
import static model.obj.cg.CgFinal.GENERATE_CODE;
import model.obj.cg.CgFinal.ReplaceBy;
import model.obj.cg.CgGeneratorRule;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.*;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulCodeGeneratorCtrl extends ZulDefaultCtrl {

    private CgCodeGeneratorBo codeGeneratorBo;
    //
    private Row rowGeneratorRule;
    private CCCombobox txtGeneratorRule;
    private Checkbox chkAllGeneratorRule;
    private Radiogroup rdgGeneratorRule;
    private Radio rdoGeneratorRuleByRule;
    private Radio rdoGeneratorRuleByField;
    //
    private Row rowEntityRule;
    private CCCombobox txtEntityRule;
    private Checkbox chkAllEntityRule;
    //
    private Textbox txtFieldName;
    private Textbox txtResult;

    @Override
    protected Class _getEntity() {
        return CgCodeGenerator.class;
    }

    @Override
    protected void _initComponent() {
        super._initComponent();
        zkView.getBtnSave().setLabel(GENERATE_CODE);
        zkView.getBtnClose().setVisible(false);
        //
        Cell cellGeneratorRule = new Cell();
        cellGeneratorRule.setParent(rowGeneratorRule);
        txtGeneratorRule.getCombobox().setParent(cellGeneratorRule);
        chkAllGeneratorRule = new Checkbox("All");
        chkAllGeneratorRule.setParent(cellGeneratorRule);
        rdgGeneratorRule = new Radiogroup();
        rdgGeneratorRule.setParent(cellGeneratorRule);
        rdoGeneratorRuleByRule = new Radio("By Rule");
        rdoGeneratorRuleByRule.setParent(rdgGeneratorRule);
        rdoGeneratorRuleByField = new Radio("By Field");
        rdoGeneratorRuleByField.setParent(rdgGeneratorRule);
        //
        Cell cellEntityRule = new Cell();
        cellEntityRule.setParent(rowEntityRule);
        txtEntityRule.getCombobox().setParent(cellEntityRule);
        chkAllEntityRule = new Checkbox("All");
        chkAllEntityRule.setParent(cellEntityRule);
        //
        setHeight(txtResult, 300);
        setWidth(txtResult, 800);
    }

    @Override
    protected void _initListener() {
        super._initListener();
        addListener(txtGeneratorRule.getCombobox(), new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                _doTxtGeneratorRule();
            }
        });
        addListener(chkAllGeneratorRule, new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                _doChkAllGeneratorRule();
            }
        });
        EventListener elRdoGeneratorRule = new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                _doRdoGeneratorRule();
            }
        };
        addListener(rdoGeneratorRuleByRule, elRdoGeneratorRule);
        addListener(rdoGeneratorRuleByField, elRdoGeneratorRule);
        addListener(chkAllEntityRule, new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                _doChkAllEntityRule();
            }
        });
    }

    @Override
    protected void _doSave() {
        if (edtEntity.isValueValid()) {
            CgCodeGenerator codeGenerator = edtEntity.getValueFromEditor();
            codeGenerator.setAllGeneratorRule(chkAllGeneratorRule.isChecked());
            codeGenerator.setGeneratorRuleByRule(rdoGeneratorRuleByRule.isChecked());
            codeGenerator.setGeneratorRuleByField(rdoGeneratorRuleByField.isChecked());
            codeGenerator.setAllEntityRule(chkAllEntityRule.isChecked());
            codeGeneratorBo.doGenerateCode(codeGenerator);
            txtResult.setText(codeGenerator.getResult());
        }
    }

    private ReplaceBy _getReplaceBy() {
        CgGeneratorRule generatorRule = txtGeneratorRule.getSelectedObject();
        if (isNotNull(generatorRule)) {
            return ReplaceBy.values()[generatorRule.getReplaceByIndex()];
        }
        return null;
    }

    private void _doTxtGeneratorRule() {
        ReplaceBy replaceBy = _getReplaceBy();
        _doUpdateAccessibleAllEntityRule(replaceBy);
        _doUpdateAccessibleEntityRule(replaceBy);
        _doUpdateAccessibleFieldName(replaceBy);
    }

    private void _doChkAllGeneratorRule() {
        edtEntity.setAccessible(txtGeneratorRule.getCombobox(), chkAllGeneratorRule.isChecked() ? Accessible.READONLY_SET_NULL : Accessible.MANDATORY);
        rdoGeneratorRuleByRule.setChecked(chkAllGeneratorRule.isChecked());
        rdoGeneratorRuleByRule.setDisabled(!chkAllGeneratorRule.isChecked());
        rdoGeneratorRuleByField.setChecked(false);
        rdoGeneratorRuleByField.setDisabled(!chkAllGeneratorRule.isChecked());
        _doRdoGeneratorRule();
    }

    private void _doRdoGeneratorRule() {
        _doUpdateAccessibleAllEntityRule(null);
        _doUpdateAccessibleEntityRule(null);
        _doUpdateAccessibleFieldName(null);
    }

    private void _doChkAllEntityRule() {
        ReplaceBy replaceBy = _getReplaceBy();
        _doUpdateAccessibleEntityRule(replaceBy);
    }

    private void _doUpdateAccessibleAllEntityRule(ReplaceBy replaceBy) {
        boolean enabled = (ReplaceBy.RULE.equals(replaceBy) || rdoGeneratorRuleByRule.isSelected());
        chkAllEntityRule.setChecked(false);
        chkAllEntityRule.setDisabled(!enabled);
    }

    private void _doUpdateAccessibleEntityRule(ReplaceBy replaceBy) {
        Accessible accessible =
                (ReplaceBy.RULE.equals(replaceBy) || rdoGeneratorRuleByRule.isSelected()) && !chkAllEntityRule.isChecked()
                ? Accessible.MANDATORY : Accessible.READONLY_SET_NULL;
        edtEntity.setAccessible(txtEntityRule.getCombobox(), accessible);
    }

    private void _doUpdateAccessibleFieldName(ReplaceBy replaceBy) {
        Accessible accessible =
                (ReplaceBy.FIELD.equals(replaceBy) || rdoGeneratorRuleByField.isSelected())
                ? Accessible.MANDATORY : Accessible.READONLY_SET_NULL;
        edtEntity.setAccessible(txtFieldName, accessible);
    }

    @Override
    protected void _doUpdateEditor() {
        super._doUpdateEditor();
        _doChkAllGeneratorRule();
    }
}