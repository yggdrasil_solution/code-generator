package controller.zul.cg;

import cococare.database.CCEntityBo;
import static cococare.datafile.CCFile.getFileUserTempFile;
import cococare.datafile.jxl.CCExcel;
import cococare.framework.zk.controller.zul.ZulDefaultCtrl;
import static cococare.zk.CCLogic.isSureExport;
import static cococare.zk.CCMessage.showExport;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.setWidth;
import static cococare.zk.datafile.CCFile.download;
import java.io.File;
import java.util.List;
import model.bo.cg.CgCodeGeneratorBo;
import model.obj.cg.CgEntityRule;
import model.obj.cg.CgEntityRuleItem;
import static model.obj.cg.CgFinal.*;
import model.obj.cg.CgGeneratorRule;
import model.obj.cg.xls.CgExportRulesToXls;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulExportRulesToXlsCtrl extends ZulDefaultCtrl {

    private CgCodeGeneratorBo codeGeneratorBo;
    //
    private Button btnExportGenRuleToXls;
    private Button btnExportEntRuleToXls;

    @Override
    protected Class _getEntity() {
        return CgExportRulesToXls.class;
    }

    @Override
    protected void _initComponent() {
        super._initComponent();
        zkView.getBtnSave().setVisible(false);
        zkView.getBtnClose().setVisible(false);
        //
        btnExportGenRuleToXls = new Button(EXPORT_GEN_RULES_TO_XLS);
        btnExportGenRuleToXls.setParent(zkView.getBtnSave().getParent());
        setWidth(btnExportGenRuleToXls, 250);
        btnExportEntRuleToXls = new Button(EXPORT_ENT_RULES_TO_XLS);
        btnExportEntRuleToXls.setParent(zkView.getBtnSave().getParent());
        setWidth(btnExportEntRuleToXls, 250);
    }

    @Override
    protected void _initListener() {
        super._initListener();
        addListener(btnExportGenRuleToXls, new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                _doBtnExportGenRuleToXls();
            }
        });
        addListener(btnExportEntRuleToXls, new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                _doBtnExportEntRuleToXls();
            }
        });
    }

    private void _doBtnExportGenRuleToXls() {
        if (isSureExport()) {
            File file = getFileUserTempFile(GENERATOR_RULES + ".xls");
            CCExcel excel = new CCExcel();
            excel.newWorkbook();
            excel.newSheet(GENERATOR_RULES);
            excel.initEntity(CgGeneratorRule.class, false);
            excel.writeRowEntityHeader();
            excel.writeRowEntity(CCEntityBo.INSTANCE.getListBy(CgGeneratorRule.class, null, null, "name ASC", 0, null));
            showExport(updateCaller = (excel.saveAndCloseWorkbook(file) && download(file)));
        }
    }

    private void _doBtnExportEntRuleToXls() {
        if (isSureExport()) {
            File file = getFileUserTempFile(ENTITY_RULES + ".xls");
            CCExcel excel = new CCExcel();
            excel.newWorkbook();
            List<CgEntityRule> entityRules = CCEntityBo.INSTANCE.getListBy(CgEntityRule.class, null, null, "name ASC", 0, null);
            for (CgEntityRule entityRule : entityRules) {
                excel.newSheet(entityRule.getName());
                excel.initEntity(CgEntityRuleItem.class, false);
                excel.writeRowEntityHeader();
                excel.writeRowEntity(codeGeneratorBo.getEntityRuleItems(entityRule));
            }
            showExport(updateCaller = (excel.saveAndCloseWorkbook(file) && download(file)));
        }
    }
}