package controller.zul.cg;

import cococare.framework.zk.controller.zul.ZulDefaultWithChildCtrl;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulEntityRuleCtrl extends ZulDefaultWithChildCtrl {

    @Override
    protected void _initComponent() {
        super._initComponent();
        _addChildScreen2("Item", "entityRule", new ZulEntityRuleItemListCtrl());
    }
}