package controller.zul.cg;

import cococare.framework.zk.controller.zul.ZulDefaultListCtrl;
import model.obj.cg.CgEntityRuleItem;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulEntityRuleItemListCtrl extends ZulDefaultListCtrl {

    @Override
    protected Class _getEntity() {
        return CgEntityRuleItem.class;
    }
}