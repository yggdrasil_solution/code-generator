package controller.zul.cg;

import cococare.common.CCFieldConfig.Accessible;
import cococare.framework.zk.controller.zul.util.ZulParameterCtrl;
import static cococare.zk.CCZk.addListener;
import model.obj.cg.CgFinal.RepeatType;
import model.obj.cg.CgFinal.ReplaceBy;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Textbox;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulGeneratorRuleCtrl extends ZulParameterCtrl {

    private Combobox txtReplaceByIndex;
    private Checkbox txtInputOnly;
    private Checkbox txtGenerateDscp;
    private Combobox txtRepeatTypeIndex;
    private Textbox txtReplacements;
    private Textbox txtFieldPattern;

    @Override
    protected void _initListener() {
        super._initListener();
        addListener(txtReplaceByIndex, new EventListener() {
            public void onEvent(Event event) throws Exception {
                _doTxtReplaceByIndex();
            }
        });
        addListener(txtRepeatTypeIndex, new EventListener() {
            public void onEvent(Event event) throws Exception {
                _doTxtRepeatTypeIndex();
            }
        });
    }

    private void _doTxtReplaceByIndex() {
        ReplaceBy replaceBy = ReplaceBy.values()[txtReplaceByIndex.getSelectedIndex()];
        Accessible accessible = ReplaceBy.RULE.equals(replaceBy) ? Accessible.MANDATORY : Accessible.READONLY_SET_NULL;
        edtEntity.setAccessible(txtInputOnly, accessible);
        edtEntity.setAccessible(txtGenerateDscp, accessible);
        edtEntity.setAccessible(txtRepeatTypeIndex, accessible);
        edtEntity.setAccessible(txtReplacements, accessible);
        edtEntity.setAccessible(txtFieldPattern, accessible);
    }

    private void _doTxtRepeatTypeIndex() {
        try {
            RepeatType repeatType = RepeatType.values()[txtRepeatTypeIndex.getSelectedIndex()];
            Accessible accessible = RepeatType.FIELDS_TO_PATTERN.equals(repeatType) ? Accessible.MANDATORY : Accessible.READONLY_SET_NULL;
            edtEntity.setAccessible(txtFieldPattern, accessible);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    protected void _doUpdateEditor() {
        super._doUpdateEditor();
        _doTxtReplaceByIndex();
        _doTxtRepeatTypeIndex();
    }
}