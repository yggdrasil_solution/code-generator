package controller.zul.cg;

import static cococare.common.CCLogic.isNotNull;
import cococare.database.CCEntityBo;
import cococare.datafile.jxl.CCExcel;
import cococare.framework.zk.controller.zul.ZulDefaultCtrl;
import static cococare.zk.CCLogic.isSureImport;
import static cococare.zk.CCMessage.showImport;
import static cococare.zk.CCZk.addListenerOnUpload;
import static cococare.zk.CCZk.setWidth;
import java.util.List;
import jxl.write.WritableSheet;
import model.obj.cg.CgEntityRule;
import model.obj.cg.CgEntityRuleItem;
import static model.obj.cg.CgFinal.*;
import model.obj.cg.CgGeneratorRule;
import model.obj.cg.xls.CgImportRulesFromXls;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Button;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulImportRulesFromXlsCtrl extends ZulDefaultCtrl {

    private Button btnImportGenRuleFromXls;
    private Button btnImportEntRuleFromXls;

    @Override
    protected Class _getEntity() {
        return CgImportRulesFromXls.class;
    }

    @Override
    protected void _initComponent() {
        super._initComponent();
        zkView.getBtnSave().setVisible(false);
        zkView.getBtnClose().setVisible(false);
        //
        btnImportGenRuleFromXls = new Button(IMPORT_GEN_RULES_FROM_XLS);
        btnImportGenRuleFromXls.setParent(zkView.getBtnSave().getParent());
        setWidth(btnImportGenRuleFromXls, 250);
        btnImportEntRuleFromXls = new Button(IMPORT_ENT_RULES_FROM_XLS);
        btnImportEntRuleFromXls.setParent(zkView.getBtnSave().getParent());
        setWidth(btnImportEntRuleFromXls, 250);
    }

    @Override
    protected void _initListener() {
        super._initListener();
        addListenerOnUpload(btnImportGenRuleFromXls, new EventListener<UploadEvent>() {
            @Override
            public void onEvent(UploadEvent event) throws Exception {
                _doBtnImportGenRuleFromXls(event);
            }
        });
        addListenerOnUpload(btnImportEntRuleFromXls, new EventListener<UploadEvent>() {
            @Override
            public void onEvent(UploadEvent event) throws Exception {
                _doBtnImportEntRuleFromXls(event);
            }
        });
    }

    private void _doBtnImportGenRuleFromXls(UploadEvent event) {
        Media media = event.getMedia();
        if (isNotNull(media)) {
            if (isSureImport()) {
                CCExcel excel = new CCExcel();
                excel.openWorkbook(media.getStreamData());
                if (isNotNull(excel.getSheet(GENERATOR_RULES))) {
                    excel.initEntity(CgGeneratorRule.class, false);
                    updateCaller = CCEntityBo.INSTANCE.getCCHibernate(CgGeneratorRule.class).restore(excel.readRowEntity(1, excel.getRowCount() - 1));
                }
                excel.closeWorkbook();
                showImport(updateCaller);
            }
        }
    }

    private void _doBtnImportEntRuleFromXls(UploadEvent event) {
        Media media = event.getMedia();
        if (isNotNull(media)) {
            if (isSureImport()) {
                CCExcel excel = new CCExcel();
                excel.openWorkbook(media.getStreamData());
                for (WritableSheet sheet : excel.getSheets()) {
                    excel.getSheet(sheet.getName());
                    CgEntityRule entityRule = new CgEntityRule();
                    entityRule.setName(sheet.getName());
                    updateCaller = CCEntityBo.INSTANCE.getCCHibernate(CgEntityRule.class).restore(entityRule);
                    //
                    excel.initEntity(CgEntityRuleItem.class, false);
                    List<CgEntityRuleItem> entityRuleItems = excel.readRowEntity(1, excel.getRowCount() - 1);
                    for (CgEntityRuleItem entityRuleItem : entityRuleItems) {
                        entityRuleItem.setEntityRule(entityRule);
                    }
                    updateCaller = CCEntityBo.INSTANCE.getCCHibernate(CgEntityRuleItem.class).restore(entityRuleItems);
                }
                excel.closeWorkbook();
                showImport(updateCaller);
            }
        }
    }
}